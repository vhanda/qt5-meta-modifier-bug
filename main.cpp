#include <QApplication>
#include <QPushButton>
#include <QKeyEvent>
#include <QDebug>

class TestClass: public QPushButton
{
protected:
    virtual void keyPressEvent(QKeyEvent* e)
    {
        uint m = e->modifiers();
        bool meta = (m & Qt::META);
        bool ctrl = (m & Qt::CTRL);
        bool alt = (m & Qt::ALT);
        qDebug() << e->key() << "Press" << "M" << meta << "C" << ctrl << "A" << alt;
    }

    virtual void keyReleaseEvent(QKeyEvent* e)
    {
        uint m = e->modifiers();
        bool meta = (m & Qt::META);
        bool ctrl = (m & Qt::CTRL);
        bool alt = (m & Qt::ALT);
        qDebug() << e->key() << "Relea" << "M" << meta << "C" << ctrl << "A" << alt;
    }

};

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    TestClass c;
    c.show();
    return app.exec();
}

